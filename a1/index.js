/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function personInfo() {
		let fullName = prompt('What is your name? ');
		let age = prompt('How old are you? ');
		let location = prompt('Where do you live? ');

		console.log('Hello, ' + fullName);
		console.log('You are ' + age + ' years old');
		console.log('You live in ' + location);

		alert('Thank you for your input!!')
	}

	personInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteArtist(){
		let artist1= ('1. Paramore');
		let artist2= ('2. Kendrick Lamar');
		let artist3= ('3. J.Cole');
		let artist4= ('4. Eminem');
		let artist5= ('5. All Time Low');

		console.log(artist1);
		console.log(artist2);
		console.log(artist3);
		console.log(artist4);
		console.log(artist5);
	}

	favoriteArtist();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies(){
		let movies1 = ('1. Harry Potter and the Sorcerer\'s Stone \nRotten Tomatoes ratings: 83% ');
		let movies2 = ('2. About Time (2013) \nRotten Tomatoes ratings: 70% ');
		let movies3 = ('3. The Hunger Games (2012) \nRotten Tomatoes ratings: 84% '); 
		let movies4 = ('4. The wolf of wall street (2013) \nRotten Tomatoes ratings: 80% '); 
		let movies5 = ('5. Silver Linings Playbook \nRotten Tomatoes ratings: 92% '); 

		console.log(movies1);
		console.log(movies2);
		console.log(movies3);
		console.log(movies4);
		console.log(movies5);
	}

	favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);